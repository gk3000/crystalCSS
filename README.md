# Crystal CSS

## A cheatsheet CSS library with most used layout/positioning elements to help you to quickly apply reusable styles. 

## 💻 Demo page: https://gk3000.gitlab.io/crystalCSS/ 

> 📬 Work in progress / open for suggestions, open an issue and let us know what layout patterns you want us to add here 

## 🛠 Installing:

Add this to your `<head>` section:

`<link rel="stylesheet" type="text/css" href="https://barcelonacodeschool.com/files/crystalCss.css">`

or download directly from https://gitlab.com/gk3000/crystalCSS/-/blob/master/public/crystalCss.css

# Contents:

## 🟢 Layout:

#### .grid2 
Grid layout with 2 equally-sized columns

#### .grid3 
Grid layout with 3 equally-sized columns

#### .grid4 
Grid layout with 4 equally-sized columns

#### .grid5 
Grid layout with 5 equally-sized columns

#### .grid6 
Grid layout with 6 equally-sized columns


## 🟢 Flex layouts

#### .flexRow
Inline no wrap 

#### .flexColumn
Column no wrap 

#### .flexRowWrap
Items inline with wrap 




## 🟢 Alignments:

#### .centMargins 
center item with margins 

#### .hcenter 
Flex horisontal alignment on the container

#### .vcenter
Flex vertical alignment on the container

#### .allcenter
Flex vertical alignment on the container


## 🟢 Containers:

#### .fullWidth 
Flex horisontal alignment on the container

#### .boxedWidth
Flex vertical alignment on the container

#### .splashView
Container taking 100% of width and height of the viewport


## 🟢 Positioning elements:

#### .fixedItem
Element fixed in the certain place

#### .stickyItem
Sticky element becomes fixed in the certain place

#### .fixedChildItem
Fixed in the certain place relative to the parent element

#### 